# Frontend Article App

This project was made using `React` and `Material-UI`. This depends on the [backend project](https://gitlab.com/ErianSeph/appbackend).
Please follow the instructions there to create a Docker image and run the whole application.