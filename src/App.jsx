import Container from '@material-ui/core/Container';
import React, { Component } from 'react';
import './App.css';
import RowController from './RowController';

export default class App extends Component {
  render() {
    return (
      <Container>
        <RowController />
      </Container>
    )
  }
}
