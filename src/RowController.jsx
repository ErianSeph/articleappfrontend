import { Box, Grid, Typography } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import { withStyles } from '@material-ui/core/styles';
import React, { Component } from 'react';
import './App.css';
import Row from './Row';
let moment = require('moment');

const axios = require('axios');

const styles = theme => ({
    box: {
        backgroundColor: 'black'
    },
    text: {
        color: 'white'
    }
})

class RowController extends Component {
    constructor() {
        super();
        this.state = {
            articles: []
        }
        this.getAllArticles = this.getAllArticles.bind(this);
        this.deleteArticle = this.deleteArticle.bind(this);
    }


    async getAllArticles() {
        let response = await axios.get('http://localhost:8000/articles', { crossDomain: true });
        return response.data;
    }

    async deleteArticle(idArticle) {
        await axios.put(`http://localhost:8000/articles/${idArticle}`,
            {
                deleted: true,
            });
        this.setState({ articles: await this.getAllArticles() });
    }

    async componentWillMount() {
        this.setState({ articles: await this.getAllArticles() });
    }

    createListItems() {
        let items = [];
        this.state.articles.map((article, i) => {
            if (article.story_title || article.title) {
                const title = article.story_title == null ? article.title : article.story_title;
                const url = article.story_url ? article.story_url : article.url;
                items.push(
                    <Row key={article._id}
                        id={article._id}
                        title={title}
                        author={article.author}
                        created_at={moment(article.created_at).fromNow()}
                        url={url}
                        deleteFunction={this.deleteArticle}
                    />
                )
            }
        })
        return items
    }

    render() {
        const { classes } = this.props;
        const rowList = this.createListItems();
        return (
            <Container>
                <Grid container>
                    <Grid item xs={12}>
                        <Box className={classes.box}>
                            <Typography className={classes.text} variant="h1">HN Feed</Typography>
                            <Typography className={classes.text} variant="h5">We {'<'}3 hacker news!</Typography>
                        </Box>
                    </Grid>
                </Grid>
                <List>
                    {rowList}
                </List>
            </Container>
        )
    }
}

export default withStyles(styles)(RowController);