import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import React, { Component } from 'react';
import './App.css';


const useStyles = theme => ({
    row: {
        backgroundColor: '#fff',
        border: 1,
        borderColor: '#ccc',
        "&:hover": {
            backgroundColor: '#fafafa'
        }
    },
    time: {
        color: '#333',
    },
    title: {
        color: '#333',
    },
    author: {
        color: '#999',
        fontSize: 13,
    },
})

class Row extends Component {

    handleRowClick = (event) => {
        const url = this.props.url;
        window.open(url, '_blank');
    }

    handleButtonClick = (event) => {
        this.props.deleteFunction(this.props.id);
    }

    render() {
        const { classes } = this.props;
        return (
            <ListItem className={classes.row}>
                <ListItemText
                    onClick={() => this.handleRowClick()}
                    primary={
                        <div className={classes.rootText}>
                            <Typography display='inline' align='left' className={classes.title}> {this.props.title} </Typography>
                            <Typography display='inline' align='left' className={classes.author}> - {this.props.author} - </Typography>
                        </div>
                    }>
                </ListItemText>
                <ListItemSecondaryAction>
                    <Typography display='inline' align='right' className={classes.time}> {this.props.created_at} </Typography>
                    <IconButton edge="end" aria-label="delete" onClick={() => this.handleButtonClick()}>
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        )
    }
}

export default withStyles(useStyles)(Row);