# First stage: builder

FROM node:latest as builder

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build 

# Second stage: 

FROM node:latest

RUN yarn global add serve
WORKDIR /app 
COPY --from=builder /app/build .
EXPOSE 3000
CMD ["serve", "-p", "3000", "-s", "."]
